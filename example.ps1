Function Get-SPOUserProfileProperty ($SiteURL, $UserID)
{
    $Cred = Get-Credential
 
    $Context = New-Object Microsoft.SharePoint.Client.ClientContext($siteUrl)
    $Context.Credentials = New-Object Microsoft.SharePoint.Client.SharePointOnlineCredentials($Cred.UserName,$Cred.Password)
    $Context.ExecuteQuery()    
 
    $RequestUrl = "$($SiteUrl)/_api/SP.UserProfiles.PeopleManager/GetPropertiesFor(accountName=@v)?@v='i:0%23.f|membership|$($UserID)'"
    $AuthenticationCookie = $Context.Credentials.GetAuthenticationCookie($SiteUrl, $true)
    $WebSession = New-Object Microsoft.PowerShell.Commands.WebRequestSession
    $WebSession.Credentials = $Context.Credentials
    $WebSession.Cookies.SetCookies($SiteUrl, $AuthenticationCookie)
    $WebSession.Headers.Add("Accept", "application/json;odata=verbose")
 
    $Result = Invoke-RestMethod -Method Get -WebSession $WebSession -Uri $RequestURL
    Return ($Result.d.userprofileproperties.results)
}
 
$SiteURL="https://dikf365.sharepoint.com/sites/IdeaBoxdev1504STAGE2NEW3"
$UserID="mprzenzak@dikf365.onmicrosoft.com"
 
Get-SPOUserProfileProperty -SiteUrl $SiteUrl -UserID $UserID | Select Key, Value