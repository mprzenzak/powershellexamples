$SiteURL="https://dikf365.sharepoint.com/sites/EnsureUser2"
$CSVPath = "C:\Users\mprze\My Drive\UserInfo site 2.csv"
 
$Cred= Get-Credential
$Ctx = New-Object Microsoft.SharePoint.Client.ClientContext($SiteURL)
$Ctx.Credentials = New-Object Microsoft.SharePoint.Client.SharePointOnlineCredentials($Cred.Username, $Cred.Password)
 
$List=$Ctx.Web.SiteUserInfoList
$FieldCollection = $List.Fields
$Ctx.Load($List)
$Ctx.Load($FieldCollection)
$Ctx.ExecuteQuery()
  
$ListItems = $List.GetItems([Microsoft.SharePoint.Client.CamlQuery]::CreateAllItemsQuery())
$Ctx.Load($ListItems)
$Ctx.ExecuteQuery()
 
$ListItemCollection = @()

ForEach($Item in $ListItems)
{
    $ExportItem = New-Object PSObject
    ForEach($Field in $FieldCollection)
    {
        $ExportItem | Add-Member -MemberType NoteProperty -name $Field.InternalName -value $Item[$Field.InternalName]  
    } 
    $ListItemCollection += $ExportItem
}
$ListItemCollection | Export-Csv -Path $CSVPath -NoTypeInformation -Force

Write-host "gotowe"